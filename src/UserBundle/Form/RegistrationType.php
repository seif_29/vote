<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use FOS\UserBundle\Util\LegacyFormHelper;

class RegistrationType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {


        $builder
                ->add('nom', null, array('label' => 'Nom', 'required' => true, 'attr' => ['class' => 'form-control']))
                ->add('prenom', null, array('label' => 'Prénom', 'required' => true, 'attr' => ['class' => 'form-control']))
                ->add('tel', null, array('label' => 'Tél', 'required' => true, 'attr' => ['class' => 'form-control']))
                ->add('email', 'email', array('label' => 'E-mail', 'attr' => ['class' => 'form-control']))
                ->add('username', null, array('label' => 'Login', 'attr' => ['class' => 'form-control']))
                ->add('plainPassword', 'repeated', array(
                    'type' => 'password',
                    'options' => array('translation_domain' => 'FOSUserBundle'),
                    'first_options' => array('label' => 'Mot de passe', 'attr' => ['class' => 'form-control']),
                    'second_options' => array('label' => 'Répété mot de passe', 'attr' => ['class' => 'form-control']),
                    'invalid_message' => 'fos_user.password.mismatch',
                ))
//                ->add('roles', null, array(
//                    'label' => 'Rôle',
//                    'type' => 'choice',
//                    'options' => array(
//                        'choices' => array(
//                            'ROLE_USER' => 'Abonné',
//                            'ROLE_ADMIN' => 'ADMIN'
//                        )
//                    )))
                ;
    }

    public function getParent() {
        return 'FOS\UserBundle\Form\Type\RegistrationFormType';

        // Or for Symfony < 2.8
        // return 'fos_user_registration';
    }

    public function getBlockPrefix() {
        return 'app_user_registration';
    }

    // For Symfony 2.x
    public function getName() {
        return $this->getBlockPrefix();
    }

}
