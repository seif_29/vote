<?php

namespace UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;

/**
 * User
 */
class User extends BaseUser
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    private $nom;

    /**
     * @var string
     */
    private $prenom;

    /**
     * @var integer
     */
    private $tel;

    /**
     * @var string
     */
    private $adr;

    /**
     * @var array
     */
    private $mails;

    /**
     * @var array
     */
    private $hobbies;

    /**
     * @var boolean
     */
    private $confirmed;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     * @return User
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set prenom
     *
     * @param string $prenom
     * @return User
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * Get prenom
     *
     * @return string 
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Set tel
     *
     * @param integer $tel
     * @return User
     */
    public function setTel($tel)
    {
        $this->tel = $tel;

        return $this;
    }

    /**
     * Get tel
     *
     * @return integer 
     */
    public function getTel()
    {
        return $this->tel;
    }

    /**
     * Set adr
     *
     * @param string $adr
     * @return User
     */
    public function setAdr($adr)
    {
        $this->adr = $adr;

        return $this;
    }

    /**
     * Get adr
     *
     * @return string 
     */
    public function getAdr()
    {
        return $this->adr;
    }

    /**
     * Set mails
     *
     * @param array $mails
     * @return User
     */
    public function setMails($mails)
    {
        $this->mails = $mails;

        return $this;
    }

    /**
     * Get mails
     *
     * @return array 
     */
    public function getMails()
    {
        return $this->mails;
    }

    /**
     * Set hobbies
     *
     * @param array $hobbies
     * @return User
     */
    public function setHobbies($hobbies)
    {
        $this->hobbies = $hobbies;

        return $this;
    }

    /**
     * Get hobbies
     *
     * @return array 
     */
    public function getHobbies()
    {
        return $this->hobbies;
    }

    /**
     * Set confirmed
     *
     * @param boolean $confirmed
     * @return User
     */
    public function setConfirmed($confirmed)
    {
        $this->confirmed = $confirmed;

        return $this;
    }

    /**
     * Get confirmed
     *
     * @return boolean 
     */
    public function getConfirmed()
    {
        return $this->confirmed;
    }
    /**
     * @var integer
     */
    private $nbqcm;


    /**
     * Set nbqcm
     *
     * @param integer $nbqcm
     * @return User
     */
    public function setNbqcm($nbqcm)
    {
        $this->nbqcm = $nbqcm;

        return $this;
    }

    /**
     * Get nbqcm
     *
     * @return integer 
     */
    public function getNbqcm()
    {
        return $this->nbqcm;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $vote;


    /**
     * Add vote
     *
     * @param \GestionBundle\Entity\Vote $vote
     * @return User
     */
    public function addVote(\GestionBundle\Entity\Vote $vote)
    {
        $this->vote[] = $vote;

        return $this;
    }

    /**
     * Remove vote
     *
     * @param \GestionBundle\Entity\Vote $vote
     */
    public function removeVote(\GestionBundle\Entity\Vote $vote)
    {
        $this->vote->removeElement($vote);
    }

    /**
     * Get vote
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getVote()
    {
        return $this->vote;
    }
}
