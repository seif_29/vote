<?php

namespace GestionBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use GestionBundle\Entity\Vote;

class VoteController extends Controller {

    public function indexAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $votes = $em->getRepository('GestionBundle:Vote')->findBy(["user" => $this->getUser()->getId()]);

        return $this->render('GestionBundle:Vote:votes.html.twig', array(
            "votes" => $votes,
        ));
    }

    public function newAction(Request $request) {

        $vote = new Vote();

        $form = $this->createForm('GestionBundle\Form\VoteType', $vote);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $vote->setCreatedate(new \DateTime());
            $vote->setUser($this->getUser());
            $vote->setParticipants(0);

            $rep1 = $request->request->get('rep1');
            $rep2 = $request->request->get('rep2');
            $rep3 = $request->request->get('rep3');

            $vote->setRep([$rep1, $rep2, $rep3]);

            $em->persist($vote);
            $em->flush();
            $this->addFlash('success', "Votre vote a été valider avec success.");
            return $this->redirectToRoute('vote_index');
        }

        return $this->render('GestionBundle:Vote:add.html.twig', array(
            'vote' => $vote,
            'form' => $form->createView(),
        ));
    }
    
    public function jointVoteAction(Request $request, $id) {
        
        $em = $this->getDoctrine()->getManager();
        $vote = $em->getRepository('GestionBundle:Vote')->find($id);



        return $this->render('GestionBundle:Vote:joint.html.twig', array(
            'vote' => $vote,

        ));
    }
    
}
