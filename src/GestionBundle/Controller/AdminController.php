<?php

namespace GestionBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class AdminController extends Controller
{
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $votes = $em->getRepository('GestionBundle:Vote')->findAll();

        return $this->render('GestionBundle:Admin:index.html.twig', [
            'votes' => $votes
        ]);
    }

}
