<?php

namespace GestionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Vote
 */
class Vote
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var \DateTime
     */
    private $createdate;

    /**
     * @var string
     */
    private $question;

    /**
     * @var array
     */
    private $rep;

    /**
     * @var integer
     */
    private $participants;

    /**
     * @var \UserBundle\Entity\User
     */
    private $user;

    public function __construct()
    {
        $this->rep = [];
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Vote
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set createdate
     *
     * @param \DateTime $createdate
     * @return Vote
     */
    public function setCreatedate($createdate)
    {
        $this->createdate = $createdate;

        return $this;
    }

    /**
     * Get createdate
     *
     * @return \DateTime 
     */
    public function getCreatedate()
    {
        return $this->createdate;
    }

    /**
     * Set question
     *
     * @param string $question
     * @return Vote
     */
    public function setQuestion($question)
    {
        $this->question = $question;

        return $this;
    }

    /**
     * Get question
     *
     * @return string 
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * Set rep
     *
     * @param array $rep
     * @return Vote
     */
    public function setRep($rep)
    {
        $this->rep = $rep;

        return $this;
    }

    /**
     * Get rep
     *
     * @return array 
     */
    public function getRep()
    {
        return $this->rep;
    }

    /**
     * Set participants
     *
     * @param integer $participants
     * @return Vote
     */
    public function setParticipants($participants)
    {
        $this->participants = $participants;

        return $this;
    }

    /**
     * Get participants
     *
     * @return integer 
     */
    public function getParticipants()
    {
        return $this->participants;
    }

    /**
     * Set user
     *
     * @param \UserBundle\Entity\User $user
     * @return Vote
     */
    public function setUser(\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \UserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
}
